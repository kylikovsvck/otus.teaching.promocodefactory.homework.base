﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task DeleteAsync(T entity)
        {
            var repo = Data as List<T>;
            var forDelete = repo.FirstOrDefault(e=>e.Id == entity.Id);
            repo.Remove(forDelete);

            return Task.CompletedTask;
        }

        public Task UpdateAsync(T entity)
        {
            var repo = Data as List<T>;
            var forDelete = repo.FirstOrDefault(e => e.Id == entity.Id);
            repo.Remove(forDelete);
            repo.Add(entity);

            return Task.CompletedTask;
        }

        public Task<IEnumerable<T>> GetByCondition(Func<T, bool> condition)
        {
            return Task.FromResult(Data.Where(condition));
        }

        public Task CreateAsync(T entity)
        {
            var data = Data as List<T>;
            data.Add(entity);
            Data = data;

            return Task.CompletedTask;
        }
    }
}
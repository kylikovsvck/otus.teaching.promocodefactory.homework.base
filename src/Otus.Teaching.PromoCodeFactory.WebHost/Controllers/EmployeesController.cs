﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;
        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Удаление сотрудника по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid id) 
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            try 
            {
                await _employeeRepository.DeleteAsync(employee);
            }
            catch(Exception e)
            {
                return BadRequest(e);
            }

            return Ok();
        }

        /// <summary>
        /// Редактирование сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <param name="employeeToSave"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateEmployeeAsync(Guid id, EmployeeToSave employeeToSave) 
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();
            try
            {
                var condition = new Func<Role, bool>(x => employeeToSave.Roles.Contains(x.Name));
                var roles = await _rolesRepository.GetByCondition(condition);

                if (!roles.Any())
                    return BadRequest("Данные роли отсутсвуют в системе");

                employee.FirstName = employeeToSave.FirstName;
                employee.LastName = employeeToSave.LastName;
                employee.Email = employeeToSave.Email;
                employee.AppliedPromocodesCount = employeeToSave.AppliedPromocodesCount;
                employee.Roles = roles.ToList();


                await _employeeRepository.UpdateAsync(employee);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }

            return Ok();
        }

        /// <summary>
        /// Создать сотрудника
        /// </summary>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployeeAsync(EmployeeToSave employeeToSave)
        {
            try
            {
                var condition = new Func<Role, bool>(x => employeeToSave.Roles.Contains(x.Name));
                var roles = await _rolesRepository.GetByCondition(condition);

                if (!roles.Any())
                    return BadRequest("Пользователь должен иметь хотябы одну роль, из ранее созданных");

                Employee employee = new Employee()
                {
                    Id = Guid.NewGuid(),
                    FirstName = employeeToSave.FirstName,
                    LastName = employeeToSave.LastName,
                    Email = employeeToSave.Email,
                    AppliedPromocodesCount = employeeToSave.AppliedPromocodesCount,
                    Roles = roles.ToList()
                };


                await _employeeRepository.CreateAsync(employee);

                return CreatedAtAction(nameof(GetEmployeeByIdAsync), new { id = employee.Id }, employee);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }            
        }
    }
}